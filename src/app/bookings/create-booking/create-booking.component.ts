import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Place } from '../../places/place.model';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.scss'],
})
export class CreateBookingComponent implements OnInit {

  @Input() selectedPlace: Place;
  @Input() selectedMode: 'select' | 'random';
  @ViewChild('bookForm') bookForm: NgForm;
  startDate: string;
  endDate: string;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    const availableFrom = new Date(this.selectedPlace.availableFrom);
    const availableTo = new Date(this.selectedPlace.availableTo);

    if (this.selectedMode === 'random') {
      this.startDate = new Date(
        availableFrom.getTime() +
        Math.random() *
        (availableTo.getTime() -
          7 * 24 * 60 * 60 * 1000 -
          availableFrom.getTime())
      ).toISOString();

      this.endDate = new Date(
        new Date(this.startDate).getTime() +
        Math.random() *
        (new Date(this.startDate).getTime() +
          6 * 24 * 60 * 60 * 1000 -
          new Date(this.startDate).getTime())
      ).toISOString();
    }
  }

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }
  onBookPlace() {
    if (!this.bookForm.valid || !this.validateDates()) {
      return;
    }

    this.modalCtrl.dismiss({
      bookingData: {
        firstName: this.bookForm.value['first-name'],
        lastName: this.bookForm.value['last-name'],
        guestNumber: +this.bookForm.value['guest-number'],
        startDate: new Date(this.bookForm.value['date-from']),
        endDate: new Date(this.bookForm.value['date-to'])
      }
    }, 'confirm');
  }
  validateDates() {
    const startDate = new Date(this.bookForm.value['date-from']);
    const endDate = new Date(this.bookForm.value['date-to']);

    return endDate > startDate;
  }
}
