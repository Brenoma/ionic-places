import { Injectable } from '@angular/core';
import { Place } from './place.model';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject, of } from 'rxjs';
import { take, map, tap, delay, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PlaceLocation } from 'src/app/places/location.model';

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
  location: PlaceLocation;
}
@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  //   new Place('p1',
  //   'Manhattan Mansion',
  //   'In the heart of New York City',
  //   'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Above_Gotham.jpg/320px-Above_Gotham.jpg',
  //   149.99,
  //   new Date('2019-01-01'),
  //   new Date('2019-12-31'),
  //   'xyz'),
  // new Place('p2',
  //   'L\'Amour Tourjours',
  //   'A Romantic Place in Paris',
  //   'https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/' +
  //   'Seine_and_Eiffel_Tower_from_Tour_Saint_Jacques_2013-08.JPG/320px-Seine_and_Eiffel_Tower_from_Tour_Saint_Jacques_2013-08.JPG',
  //   189.99,
  //   new Date('2019-01-01'),
  //   new Date('2019-12-31'),
  //   'xyz'),
  // new Place('p3',
  //   'The Foggy Palace',
  //   'Not Your Average city trip',
  //   'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/' +
  //   'San_Francisco_from_the_Marin_Headlands_in_March_2019.jpg/320px-San_Francisco_from_the_Marin_Headlands_in_March_2019.jpg',
  //   99.99,
  //   new Date('2019-01-01'),
  //   new Date('2019-12-31'),
  //   'xyz')
  // tslint:disable-next-line: variable-name
  private _places = new BehaviorSubject<Place[]>([]);

  get places() {
    return this._places.asObservable();
  }
  constructor(private authService: AuthService, private http: HttpClient) { }
  getPlace(id: string) {
    return this.http.get<PlaceData>(`https://ionic-course-project-7cc9e.firebaseio.com/offered-places/${id}.json`
    ).pipe(
      map(placeData => {
        return new Place(id, placeData.title,
          placeData.description,
          placeData.imageUrl,
          placeData.price,
          new Date(placeData.availableFrom),
          new Date(placeData.availableTo),
          placeData.userId,
          placeData.location);
      })
    );
  }
  fetchPlaces() {
    return this.http.get<{ [key: string]: PlaceData }>('https://ionic-course-project-7cc9e.firebaseio.com/offered-places.json')
      .pipe(map(resData => {
        const places = [];
        for (const key in resData) {
          if (resData.hasOwnProperty(key)) {
            places.push(new Place(key,
              resData[key].title,
              resData[key].description,
              resData[key].imageUrl,
              resData[key].price,
              new Date(resData[key].availableFrom),
              new Date(resData[key].availableTo),
              resData[key].userId,
              resData[key].location));
          }
        }
        return places;
        // return [];
      }),
        tap(places => {
          this._places.next(places);
        }));
  }
  addPlace(title: string, description: string, price: number, dateFrom: Date, dateTo: Date, location: PlaceLocation) {
    let generatedId: string;
    const newPlace = new Place(Math.random().toString(), title, description,
      'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Above_Gotham.jpg/320px-Above_Gotham.jpg',
      price, dateFrom, dateTo, this.authService.userId, location);

    return this.http.post<{ name: string }>('https://ionic-course-project-7cc9e.firebaseio.com/offered-places.json', {
      ...newPlace, id: null
    }).pipe(
      switchMap(resData => {
        generatedId = resData.name;
        return this.places;
      }),
      take(1),
      tap(places => {
        newPlace.id = generatedId;
        this._places.next(places.concat(newPlace));
      })
    );
  }
  updatePlace(placeId: string, title: string, description: string) {
    let updatedPlaces: Place[];
    return this.places.pipe(
      take(1),
      switchMap(places => {
        if (!places || places.length <= 0) {
          return this.fetchPlaces();
        } else {
          return of(places);
        }
      }), switchMap(places => {
        const updatedPlaceIndex = places.findIndex(pl => pl.id === placeId);
        updatedPlaces = [...places];
        const oldPlace = updatedPlaces[updatedPlaceIndex];
        updatedPlaces[updatedPlaceIndex] = new Place(
          oldPlace.id,
          title,
          description,
          oldPlace.imageUrl,
          oldPlace.price,
          oldPlace.availableFrom,
          oldPlace.availableTo,
          oldPlace.userId,
          oldPlace.location);
        return this.http.put(`https://ionic-course-project-7cc9e.firebaseio.com/offered-places/${placeId}.json`,
          { ...updatedPlaces[updatedPlaceIndex], id: null });
      }), tap(resData => {
        this._places.next(updatedPlaces);
      })
    );
  }
}
